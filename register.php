<?php
require_once("vendor/autoload.php");
use PHPMailer\PHPMailer\PHPMailer;

session_start();
if(isset($_POST['signup'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $email = $_POST['email'];

    $create_table_query = "CREATE TABLE IF NOT EXISTS $username(`id` int PRIMARY KEY AUTO_INCREMENT,`image_title` varchar(255), `date` date ,`location` varchar(255), `description` varchar(255), `image_ext` varchar(255));";

    $create_user_query = "INSERT INTO `users` values(NULL, '$email', '$username', '$password');";
    $dsn = "mysql:host=localhost;port=3306;dbname=photo";

    $pdo = new PDO($dsn,'zaid','abcd1234');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo->exec($create_table_query);
    $pdo->exec($create_user_query);

    mkdir("./uploaded_images/$username");

    $_SESSION['username'] = $username;
    try{
      $mail = new PHPMailer();
      $mail->isSMTP();                                            //Send using SMTP
      $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
      $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
      $mail->Username   = 'zakazi6113@gmail.com';                     //SMTP username
      $mail->Password   = 'nxvjxhfxfqknwlqq';                               //SMTP password
      // $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
      $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
      $mail->SMTPSecure = 'ssl';

      //Recipients
      $mail->setFrom('info@blog.com', 'Photo Blog Website');
      $mail->addAddress($email, $username);     //Add a recipient

      $mail->isHTML(true);                                  //Set email format to HTML
      $mail->Subject = 'Successful Registration!';
      $mail->Body    = "<h2>Dear $username, Welcome to our website<br>You can now login!</h2>";

      $mail->send();
    }catch(Exception $e){
      die($e);
    }
    header("Location: index.php");

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Photography Website</title>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles/login-style.css">
  <link rel="stylesheet" href="styles/register-style.css">
</head>
<body>
  <div class="container">
    <div class="login-container">
    <div class="login-form">
      <!-- <p class="login-caption">Logi</p> -->
      <h2 style="text-align: center;">Sign Up!</h2>
      <form action="<?=$_SERVER['PHP_SELF']?>" method="POST">
        <input name = "username" type="text" placeholder="Username" required class = "form-inputs">
        <input name = "email" type="text" placeholder="Email" required class = "form-inputs">
        <input name = "password" type="password" placeholder="Password" required class = "form-inputs">
        <div class="login-btn-holder form-inputs">
          <button type="submit" name="signup">Submit</button>
        </div>
      </form>
      <p>Already have an account? <a href="index.php">Log In</a></p>
    </div>
  </div>
  </div>
</body>
</html>
