var deleteButtons = document.querySelectorAll(".delete-btn");

deleteButtons.forEach(function(deleteBtn){
    // console.log(deleteBtn);
    deleteBtn.addEventListener('click', confirmDeletion);
});

function confirmDeletion(e) {
    e.preventDefault();

    // console.log("Inside");
    var check = confirm("Do you want to delete the post?");
    if(check){
        var parentForm = this.parentElement;
        var hidden = document.createElement("input");
        hidden.type = "hidden";
        hidden.name = "delete-id";
        hidden.value = this.dataset.id;
        parentForm.appendChild(hidden);
        parentForm.submit();
    }
    else{
        return;
    }
}