<?php
session_start();
if(isset($_SESSION['username'])){
  require_once "functions.php";
  $username = $_SESSION['username'];
  $dsn = "mysql:host=localhost;port=3306;dbname=photo";
  $pdo = new PDO($dsn,'zaid','abcd1234');

  $sql = "SELECT * FROM $username;";
  $ps = $pdo->prepare($sql);
  $ps->execute();
  $rows = [];

  while($row = $ps->fetch(PDO::FETCH_ASSOC)) {
    $rows[] = $row;
  }
  // die(var_dump($rows));
}
else {
  die(var_dump("Some error occurred!"));
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Photography Website - Dashboard</title>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">


  <link rel="stylesheet" href="styles/styles.css">
  <link rel="stylesheet" href="styles/dashboard-style.css">
</head>
<body>
  

  
  <div class="navbar">
    <p class="nav-header nav-items">Hello, <?=$_SESSION['username']?></p>
    <p class="nav-header nav-items">Your photo blog!</p>
    <form action="logout.php" method="POST" style="display: flex">
      <button class="logout-btn nav-items" name="logout">Logout</button>
    </form>
  
  </div>
  <div class="container-fluid">
    <?php
      if(count($rows) > 0):
        for ($i=0; $i < count($rows); $i++):
    ?>
    <!-- Card for each photo -->
    <div class="photo-card">
        <h3 id = "card-title"><?=$rows[$i]['image_title']?></h3>
        <div class="card-items card-elements img-holder-card">
        <?php
        $image = $rows[$i]['id'].'.'.$rows[$i]['image_ext'];
        ?>
        <img src="uploaded_images/<?=$username?>/<?=$image?>" alt="Image" width="100%" height="100%">
        </div>
        <div class="card-info">
            <p class="card-items card-elements">Pic Uploaded On: &nbsp;<?=getFormattedDate($rows[$i]['date'])?></p>
            <p class="card-items card-elements">Location: <?=$rows[$i]['location']?></p>
            <p class="card-items card-elements">Description: <?=$rows[$i]['description']?></p>
            <form action="deletepost.php" method="POST">
            <button class="delete-btn" data-id = "<?=$rows[$i]['id']?>"><i class="bi bi-trash"></i><span class = "delete-txt">Delete</span></button>
            </form>
            
        </div>
      
      </div>
    <?php
      endfor;
      ?>
      <?php
      endif
    ?>
    </div>
    <!-- Floating action button -->
    <form action="addphoto.php" method = "POST">
      <button class="add-photo-btn" name="add">+</button>
    </form>
  </div>
  <!-- <button data-bs-target="deleteModal">Try</button> -->
  
  <script src="js/dashboard.js"></script>
</body>
</html>
