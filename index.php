<?php
session_start();
  $error = false;
  if(isset($_SESSION['username'])) {
    header("Location: dashboard.php");
  }
  if(isset($_POST['login'])) {
    // var_dump($_POST);
    // die();
    $user = $_POST['username'];
    $password = $_POST['password'];

    $dsn = "mysql:host=localhost;port=3306;dbname=photo";
    $pdo = new PDO($dsn,'zaid','abcd1234');

    $sql = "SELECT COUNT(*) AS count FROM `users` WHERE `username` = ? AND `password` = ?";
    $ps = $pdo->prepare($sql);
    $ps->execute([$user, $password]);
    $records = $ps->fetchAll(PDO::FETCH_ASSOC)[0]['count'];
    if($records == 0) {
      $error = true;
      // echo "Invalid username/password";
    } else {
      $_SESSION['username'] = $user;
      header("Location: dashboard.php");
      // echo "valid username/password";
    }
    // die();

  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Photography Website</title>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles/login-style.css">
</head>
<body>
  <div class="container">
    <div class="login-container">
    <div class="login-form">
      <!-- <p class="login-caption">Logi</p> -->
      <h2 style="text-align: center;">Login</h2>
      <form action="<?=$_SERVER['PHP_SELF']?>" method="POST">
        <input name = "username" type="text" placeholder="Username" required class = "form-inputs">
        <input name = "password" type="password" placeholder="Password" required class = "form-inputs">
        <div class="login-btn-holder form-inputs">
          <button type="submit" name="login">Login</button>
        </div>
      </form>
      <?php
        if($error):
      ?>
        <span class = "error-message">Invalid Login Credentials</span>
      <?php
        endif;
      ?>
      <p>Don't have an account? <a href="register.php">Sign Up</a></p>
    </div>
  </div>
  </div>
</body>
</html>
