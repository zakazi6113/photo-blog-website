<?php

  session_start();
  if(isset($_POST['add-img'])) {

    // echo "Inside...<br>";
    // var_dump($_POST);
    $title = $_POST['title'];
    $location = $_POST['location'];
    $description = $_POST['desc'];
    $username = $_SESSION['username'];
    $filePath = $_FILES['place-img']['tmp_name'];
    $dsn = "mysql:host=localhost;port=3306;dbname=photo";
    $pdo = new PDO($dsn,'zaid','abcd1234');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // echo $pdo->getAttribute(PDO::ATTR_ERRMODE);
    // $lastId = $pdo->lastInsertId("");
    // die(var_dump($_FILES));
    $fullPath = $_FILES['place-img']['full_path'];
    // die(var_dump($fullPath));
    $image_ext = end(explode(".", $fullPath));
    // die(var_dump(explode(".", $fullPath)));
    $con = mysqli_connect("localhost","zaid","abcd1234","photo","3306");
    $date = date('Y-m-d');
    $query = "INSERT INTO $username (`image_title`, `date`, `location`, `description`, `image_ext`) VALUES ('$title', '$date', '$location', '$description', '$image_ext');";
    // die(var_dump($query));
    $pdo->exec($query);
    // echo("Here");
    $lastId = $pdo->lastInsertId();
    $imageName = $lastId.'.'.$image_ext;
    move_uploaded_file($filePath, "./uploaded_images/$username/$imageName");
    header("Location: dashboard.php");
    // var_dump($con);
    // die(var_dump(mysqli_insert_id($con)));
  
  } 

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Photography Website - Add Photo</title>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="styles/styles.css">
  <link rel="stylesheet" href="styles/add-form-style.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="navbar">
    <p class="nav-header nav-items"><a href="dashboard.php"><i class="bi bi-chevron-left" style="margin-right: 20px; font-weight:600;"></i></a>Hello, <?=$_SESSION['username']?></p>
    <p class="nav-header nav-items">Try adding some amazing photos to your blog!</p>
    <form action="logout.php" method="POST" style="display: flex">
      <button class="logout-btn nav-items" name="logout">Logout</button>
    </form>
  
  </div>
  <div class="add-form-container">
    <div class="add-photo-form">
      <h2 id = "add-card-title">Add Post to your blog!</h2>
    
      <form action="<?=$_SERVER['PHP_SELF']?>" method="POST" enctype="multipart/form-data">
        <input type="text" placeholder="Title of Image" name = "title" required class = "add-form-controls">
        <div class="image-uploader" class = "add-form-controls">
          <!-- Image uploader div -->
          <input type="text" placeholder="Selected image name" id = "selected-image" disabled>
          <label for="image-upload">Upload Image</label>
          <input name = "place-img"  type="file" id="image-upload" accept="image/*" required>
        </div>
        <input type="text" name = "location" placeholder="Location" required class = "add-form-controls">
        <textarea style="min-height: 60px;" name = "desc" placeholder="Description" required class = "add-form-controls"></textarea>
        <button type="submit" name = "add-img" class = "add-form-controls">Upload The Post!</button>
      </form>
    </div>
  </div>
  <script src="js/addphoto.js"></script>
</body>
</html>
